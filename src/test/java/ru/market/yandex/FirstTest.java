package ru.market.yandex;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class FirstTest {

    public ChromeDriver driver;

    @Before
    public void browser() {
        System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void firstTest() {

        driver.get("https://google.com/");

        WebElement enter = driver.findElement(By.xpath("//input[@name='q']"));
        enter.sendKeys("яндекс маркет");
        enter.sendKeys(Keys.ENTER);

        WebElement enterButton = driver.findElement(By.xpath("//h3[contains(text(),'Яндекс.Маркет — выбор и покупка товаров из провере')]"));
        enterButton.click();

        enter = driver.findElement(By.xpath("//input[@id='header-search']"));
        enter.sendKeys("пылесосы");
        enter.sendKeys(Keys.ENTER);

        enterButton = driver.findElement(By.xpath("//span[contains(text(),'Пылесосы')]"));
        enterButton.click();

        enterButton = driver.findElement(By.xpath("//span[contains(text(),'Все фильтры')]"));
        enterButton.click();

        enterButton = driver.findElement(By.xpath("//label[contains(text(),'Polaris')]"));
        enterButton.click();

        enterButton = driver.findElement(By.xpath("//label[contains(text(),'VITEK')]"));
        enterButton.click();

        enter = driver.findElement(By.xpath("//input[@id='glf-priceto-var']"));
        enter.sendKeys("6000");

        enterButton = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[5]/div[1]/div[1]/div[5]/a[2]"));
        enterButton.click();

    }
    @After
     public void close() {
    //   driver.quit();
    }
}
